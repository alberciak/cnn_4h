
import org.apache.log4j.BasicConfigurator;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class Glasses {
    private static final Logger log = LoggerFactory.getLogger(Glasses.class);

    public static void main(String[] args) throws Exception {

        BasicConfigurator.configure();
        int w = 90;
        int h = 100;

        File trainFolder = new File("D:\\PRIV\\repos\\deeplearning\\glasses\\data\\train");
        File testFolder = new File("D:\\PRIV\\repos\\deeplearning\\glasses\\data\\test");

        DataSetIterator trainData = getDataSetIterator(trainFolder, 32, h, w);
        DataSetIterator testData = getDataSetIterator(testFolder, 128, h, w);

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .iterations(2)
                .learningRate(.001)
                .list()
                .layer(0, new ConvolutionLayer.Builder(3, 3)
                        .nIn(1)
                        .stride(1, 1)
                        .nOut(8)
                        .activation(Activation.RELU)
                        .build())
                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(4,4)
                        .stride(4,4)
                        .build())
                .layer(2, new ConvolutionLayer.Builder(5, 5)
                        .stride(1, 1)
                        .nOut(4)
                        .activation(Activation.RELU)
                        .build())
                .layer(3, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(2,2)
                        .stride(2,2)
                        .build())
                .layer(4, new ConvolutionLayer.Builder(5, 5)
                        .stride(1, 1)
                        .nOut(8)
                        .activation(Activation.RELU)
                        .build())
                .layer(5, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(4,4)
                        .stride(4,4)
                        .build())
                .layer(6, new DenseLayer.Builder().activation(Activation.RELU)
                        .nOut(50).build())
                .layer(7, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(2)
                        .activation(Activation.SOFTMAX)
                        .build())
                .setInputType(InputType.convolutionalFlat(h, w,1)) //See note below
                .build();

        MultiLayerNetwork network = new MultiLayerNetwork(conf);
        network.init();
        network.setListeners(new ScoreIterationListener(10)); //Print score every 10 iterations

        for( int ii=0; ii<100; ii++ ) {
            log.info("Epoch {}", ii);
            network.fit(trainData);
            Evaluation eval = network.evaluate(testData);
            log.info(eval.stats());
        }
    }


    private static DataSetIterator getDataSetIterator(File folder, int batchSize, int h, int w) throws IOException {
        Random randNumGen = new Random();
        FileSplit files = new FileSplit(folder, NativeImageLoader.ALLOWED_FORMATS, randNumGen);
        ParentPathLabelGenerator labelGenerator = new ParentPathLabelGenerator();
        ImageRecordReader recordReader = new ImageRecordReader(h, w, 1, labelGenerator);
        recordReader.initialize(files);
        return new RecordReaderDataSetIterator(recordReader, batchSize, 1, 2);
    }
}
