import cv2
import numpy as np


def zoom_and_show(name, image):
    zoom = 10
    zoomed = cv2.resize(image, dsize=None, fx=zoom, fy=zoom, interpolation=0)
    zoomed[zoomed<0]=0
    zoomed = (zoomed* 255.0).astype(np.uint8)
    cv2.imshow(name, zoomed)


def convolution(image, filt):
    H0, W0, C0 = image.shape
    h, w, c = filt.shape
    H1, W1 = (H0-h+1, W0-w+1)
    result = np.zeros(shape=(H1, W1), dtype=np.float32)

    for y in range(H1):
        for x in range(W1):
            result[y,x]=np.sum(np.multiply(image[y:y+h,x:x+w,:],filt[:,:,:]))
    return result


image = cv2.imread('shapes.png').astype(np.float32)/255.0
zoom_and_show('original', image)


o = np.array([0,0,0])
w = np.array([1,1,1])
r = np.array([0,0,1])
g = np.array([0,1,0])
b = np.array([1,0,0])

f1 = np.array([
    [o, o, o],
    [o, o, o],
    [o, o, o]
], dtype=np.float32)

im_f1 = convolution(image, f1)
zoom_and_show('im_right', im_f1)

if False:
    f2 = -f1
    f3 = np.transpose(f2, axes=(1, 0, 2))
    f4 = -f3

    im_f2 = convolution(image, f2)
    zoom_and_show('im_left', im_f2)

    im_f3 = convolution(image, f3)
    zoom_and_show('im_top', im_f3)

    im_f4 = convolution(image, f4)
    zoom_and_show('im_bot', im_f4)


    edges = np.stack([im_f2, im_f1, im_f3], axis=2) * 6
    zoom_and_show('edges', edges)

cv2.waitKey()