# PART 2 - Understanding convolution

The aim of this exercise is to play with convolution

1. Run playground_step0 to see 2 images (input and output of convolution)
2. Experiment with the contents of f1 numpy array and see what happens
3. Try to smooth the input image
4. Try to detect left edges
5. Try to detect top edges
6. Try to detect only red edges
7. Run the playground_step1 script. What does it do?
7. Can you think of a method to detect the center of the square?