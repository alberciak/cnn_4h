from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D, GlobalMaxPool2D
from keras.layers import Activation, Dropout, Flatten, Dense, BatchNormalization, Softmax
import keras.optimizers
from keras import regularizers
from datetime import datetime

batch_size = 32

#1 Try to train the network with this really shallow architecture
#2 Check different optimization method
#3 Add more convolutional and pooling layers to the network
#4 Add regularization
#5 Add dropout
#6 Augment the dataset
#7 Switch your network to fully convolutional architecture.
	# - remove fully connected part
	# - add convolution, with global average pooling
#8 Add batch norm

train_datagen = ImageDataGenerator(
        rescale=1./255 #,
        # shear_range=0.2,
        # zoom_range=0.2,
        # horizontal_flip=True
     )

test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
        '../data/train',  # this is the target directory
        target_size=(200, 180),  # all images will be resized to 150x150
        batch_size=batch_size
)

validation_generator = test_datagen.flow_from_directory(
        '../data/test',
        target_size=(200, 180),
        batch_size=batch_size
)


model = Sequential()
model.add(Conv2D(16, (3, 3), input_shape=(200, 180, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(8, 8)))

model.add(Conv2D(16, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(8, 8)))

model.add(Flatten())
model.add(Dense(16))
model.add(Activation('relu'))
model.add(Dense(2))
model.add(Activation('softmax'))

opt = keras.optimizers.Adam()

model.compile(loss='binary_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])

now = datetime.now()
logname = now.strftime("%Y%m%d-%H%M%S") + "/"
logdir = "tensor_board_logs/" + logname

callbacks = [
    keras.callbacks.ModelCheckpoint('checkpoints/01epoch.{epoch:02d}-{val_acc:.2f}.hdf5', monitor='val_acc',
                                    verbose=0, save_best_only=True, save_weights_only=False, mode='auto', period=1),
    keras.callbacks.TensorBoard(log_dir=logdir, batch_size=batch_size, write_graph=True, write_grads=True)
]

model.fit_generator(
        train_generator,
        steps_per_epoch=(1237 * 2) / batch_size,
        epochs=50,
        validation_data=validation_generator,
        validation_steps=(138 * 2) / batch_size,
        callbacks=callbacks)

