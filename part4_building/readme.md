# PART 4 - Building, training and improving CNN with Keras #

The aim of this exercise is to:

1. Understand what influences network training
2. Understand significance of model size, training data volume, validation and test datasets.
3. Understand bias/variance, over/under-fitting
4. Learn how to improve the network with new layers, regularization and data augmentation
5. Learn how to turn a network into fully convolutional and understand the benefits
6. Learn how to interpret the results, and check where the network is failing
7. Learn Keras framework during the process

## Exercises

Start with train_glasses_step00.py. Run the script.
Meanwhile, in your terminal run: tensorboard --logdir path_to_repo/part4_building/tensor_board_logs
(if tensorboard fails with invalid string format, it is a small bug of tensorflow. Just change %s to %S in the failing line)
In your browser go to http://localhost:6006. Monitor your training process.

Analyse the code. Instructions are there within the code. Try to follow them with instructor.