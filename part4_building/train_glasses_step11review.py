from keras.models import load_model
import cv2
import numpy as np
import os
from random import shuffle

test_folder = '../data/test'
model = load_model('checkpoints/epoch2.20-0.98.hdf5')
review_all = False

data = []
for c in [0, 1]:
    dir = os.path.join(test_folder, str(c))
    files = os.listdir(dir)
    for f in files:
        img = cv2.imread(os.path.join(dir, f))
        data.append((img, c))
shuffle(data)

for d in data:
    img = d[0]
    creal = d[1]==1
    tensor = img.astype(np.float32) / 255
    tensor = tensor[:, :, [2, 1, 0]]
    tensor = np.array([tensor])
    result = model.predict(tensor)
    cpred = result[0][1] > result[0][0]
    if cpred != creal or review_all:
        prob = result[0][1]*100
        cv2.putText(img, "{0:.2f}%".format(prob), (10,50), cv2.FONT_HERSHEY_PLAIN, 2, (0,0,255), 2)
        cv2.imshow('input', img)
        print("real: {}, predicted: {}".format(creal, cpred))
        cv2.waitKey()

