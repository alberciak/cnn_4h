# PART 5 - Applying CNN

1. Start with use_glasses_step0.py. It displays your video stream.
2. Choose one of your saved checkpoints from part 4, copy it to the part5 folder and rename as trained_model.hdf5.
3. Load the model (from keras.models import load_model) and try to run predict function on it with your image as input (try to solve the errors by proper preparation of your image as input to the model)
4. Try to display the intermediate feature vector 