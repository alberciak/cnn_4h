import sys
import cv2
from keras.models import load_model
import numpy as np

# initialize capturing video using from commandline argument, eg:
# 0                                 - capture video from the first internal video device, usually a webcam
# video.mp4                         - capture video from file
# http://192.168.43.1:8080/video    - capture video from online video stream
try:
    camID = int(sys.argv[1])
except:
    camID = sys.argv[1]
cap = cv2.VideoCapture(camID)

model = load_model('trained_model.hdf5')

# run detection on the video stream
while True:
    ret, img = cap.read()
    if ret == 0:
        break

    (H, W, C) = (200, 180, 3)
    (h, w, c) = img.shape
    img = img[int(h/2-H/2):int(h/2+H/2), int(w/2-W/2):int(w/2+W/2), :]


    tensor = img.astype(np.float32) / 255
    tensor = tensor[:, :, [2, 1, 0]]
    tensor = np.array([tensor])
    result = model.predict(tensor)
    cpred = result[0][1]

    cv2.putText(img, "{}%".format(str(int(result[0][1] * 100))), (10, 50), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255), 2)

    cv2.imshow("Face detection", img)
    k = cv2.waitKey(1) & 0xff
    if k == ord('q') or k == 27:
        break

cap.release()
