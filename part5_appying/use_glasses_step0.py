import sys
import cv2
from keras.models import load_model
import numpy as np

# initialize capturing video using from commandline argument, eg:
# 0                                 - capture video from the first internal video device, usually a webcam
# video.mp4                         - capture video from file
# http://192.168.43.1:8080/video    - capture video from online video stream
try:
    camID = int(sys.argv[1])
except:
    camID = sys.argv[1]
cap = cv2.VideoCapture(camID)

# run detection on the video stream
while True:
    ret, img = cap.read()
    if ret == 0:
        break

    cv2.imshow("Face detection", img)
    k = cv2.waitKey(1) & 0xff
    if k == ord('q') or k == 27:
        break

cap.release()
