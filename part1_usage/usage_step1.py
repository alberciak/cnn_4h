#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=C0103
# pylint: disable=E1101

import sys
import cv2
import tfdetector

# initialize capturing video using from commandline argument, eg:
# 0                                 - capture video from the first internal video device, usually a webcam
# video.mp4                         - capture video from file
# http://192.168.43.1:8080/video    - capture video from online video stream
try:
    camID = int(sys.argv[1])
except:
    camID = sys.argv[1]
cap = cv2.VideoCapture(camID)

# load a trained network
PATH_TO_MODEL = './model/model.pb'
PATH_TO_LABELS = './model/model.pbtxt'
tDetector = tfdetector.TensoflowFaceDector(PATH_TO_MODEL, PATH_TO_LABELS)

# run detection on the video stream
while True:
    ret, image = cap.read()
    if ret == 0:
        break

    result = tDetector.run(image)
    tDetector.draw_detection(image, result)

    cv2.imshow("Face detection", image)
    k = cv2.waitKey(1) & 0xff
    if k == ord('q') or k == 27:
        break

cap.release()
