# PART 1 - using online resources

The aim of this exercise is to use existing neural network trained for face detection

1. Launch 'usage_step0.py 0' to run sample application that will connect to main camera in your laptop and display your video stream
2. Review the contents of model directory. 
3. Use TensoflowFaceDector class from tfdetector package (constructor, run and draw_detection methods) to apply detection of faces to your video stream
3. Check the reference solution in usage_step1.py
