import tensorflow as tf
import numpy as np
import cv2
import os

### build the graph
images_placeholder = tf.placeholder(tf.float32, shape=[None, 200, 180, 1], name="images")
labels_placeholder = tf.placeholder(tf.uint8, shape=[None, 2], name="labels")

edge_detector = Filters.edge(name='edge')
frame_detector = Filters.frame(name='frame')
glasses_detector = Filters.glasses(name='glasses')
glass_classifier = Filters.classifier(name='classifier')

result = images_placeholder
result = filter_and_zoomout(inp=result, filt=edge_detector, zoom=10)
result = filter_and_zoomout(inp=result, filt=frame_detector, zoom=2)
result = filter_and_zoomout(inp=result, filt=glasses_detector, zoom=2)
result = classify(inp=result, filt=glass_classifier)
accuracy, cost = calculate_metrics(result, labels_placeholder)

### load data
loader = Loader('data/train')
batch = 4
loader.load_datasets(batch=batch)

### run the graph
sess = tf.Session()
imgs, labs = loader.get_training_examples(sess)
train_dict = {images_placeholder: imgs, labels_placeholder: labs}

### show results
print_metrics(sess, accuracy, cost, data_to_feed=train_dict)
display_results(sess, data_to_feed=train_dict)