# PART 0 - Preparation

In order to save your time during the workshop, we advise you to complete the most download/time critical part before the workshop.
First of all :

1. download/clone this repository.
2. go through the steps in part0_environment/python_preparation.txt